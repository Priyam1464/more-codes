package JDBC;

//import com.mysql.jdbc.Connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CricketDAO {
   private Connection connection= DatabaseConnection.getConnection();
    public int insertPlayer(PlayerBean playerBean) throws SQLException {

        String firstName=playerBean.getFirstName();
        String lastName=playerBean.getLastName();
        int id=playerBean.getPlayerId();
        int jerseyNo=playerBean.getJerseyNo();
      //  String query="INSERT INTO Player VALUES("+String.valueOf(id)+","+firstName+","+lastName+","+String.valueOf(jerseyNo)+")";
        PreparedStatement preparedStatement=  connection.prepareStatement("INSERT INTO Players(id,firstname,lastname,jerseyno) VALUES (?,?,?,?) ");
         preparedStatement.setInt(1,id);
         preparedStatement.setString(2,firstName);
         preparedStatement.setString(3,lastName);
         preparedStatement.setInt(4,jerseyNo);
         //connection.close();
        return preparedStatement.executeUpdate();
    }
    
}
