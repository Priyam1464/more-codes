package JDBC;

public class PlayerBean {
    private  int playerId,jerseyNo;
    private String firstName,lastName;
    //private static Connection connection=null;
     public PlayerBean(int playerId,String firstName,String lastName,int jerseyNo)
     {
         this.playerId=playerId;
         this.firstName=firstName;
         this.lastName=lastName;
         this.jerseyNo=jerseyNo;
     }

    public int getJerseyNo() {
         return jerseyNo;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


}
