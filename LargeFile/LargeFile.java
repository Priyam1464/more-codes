package LargeFile;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LargeFile {

    public String generateName()
    {
        Random random=new Random();
        int length=random.nextInt(5)+5;
        String s="";
        char[] c={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        for(int i=0;i<length;i++)
        {
            int index=random.nextInt(26);
            s+=c[index];
        }
        return s;
    }
    public int generateAge()
    {
        Random random=new Random();
        int age=random.nextInt(100)+1;
        return age;
    }

    public String generatePhoneNumber()
    {
        Random random=new Random();
        String phoneNumber = "";
        int length=10;
        for(int i=0;i<length;i++) {
            int num = random.nextInt(9) + 1;

            phoneNumber += String.valueOf(num);
        }
        return phoneNumber;
    }
    public static void main(String[] args) {
        try
        {
            FileWriter fw=new FileWriter("sampledata.csv");
            BufferedWriter bufferedWriter=new BufferedWriter(fw);
            Map<BigInteger,Boolean > map=new HashMap<>();
          //  fw.write("Name Age,PhoneNumber\n");
            String fileSize ="4294967296";
        long size=Long.parseLong(fileSize);
            //Random  random=new Random();
          while(size>0)
          {
              LargeFile largeFile=new LargeFile();
                String name=largeFile.generateName();
                int age=largeFile.generateAge();
                HashMap<String,Boolean> mobileNumberMap=new HashMap<>();
                String phoneNumber=largeFile.generatePhoneNumber();
                while (mobileNumberMap.containsKey(phoneNumber))
                {
                    phoneNumber=largeFile.generatePhoneNumber();
                    mobileNumberMap.put(phoneNumber,true);
                }
                String output=name+" "+age+" "+phoneNumber+"\n";
                bufferedWriter.write("Name "+"Age "+"Mobile Number"));
                bufferedWriter.write(output);
                //fw.write(output);
              bufferedWriter.newLine();
                size-=output.getBytes().length;
          }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
